require 'uri'
require 'json'
require 'net/http'
require 'digest/md5'
require 'dotenv'

class User

    def initialize(params)
        @email = params['email']
        @password = params['password']
    end

    def self.http_request(uri, type, body = nil)
        url = URI(uri)

        https = Net::HTTP.new(url.host, url.port)
        https.use_ssl = true

        request_string =  "Net::HTTP::#{type}.new(url)"
        request = eval(request_string)

        request["Content-Type"] = 'application/json'
        request["Site"] = 'PostmanMahlatse'
        request['UserID'] = 326
        request.body = body if body.present?


        response = https.request(request)
        JSON.parse(response.read_body)
    end

    def self.request_type
        {
            post: :Post,
            get: :Get,
            put: :Put,
            delete: :Delete
        }
    end

    def self.authentication(email, password)
        passw_rd = Digest::MD5.hexdigest(password)
        
        body = "{\n 
                \"email\": \"#{email}\",
                \"password\": \"#{passw_rd}\",
                \n}"

         User.http_request(
             ENV['USER_AUTH'],
             User.request_type[:post],
             body
         )
    end

    def identities
        response = User.http_request(
            ENV['IDENTITY'], 
            User.request_type[:get]
            )
    end

    def get_identity(identityID)
        response = User.http_request(
            "#{ENV['IDENTITY']}/#{identityID}", 
            User.request_type[:get]
            )
    end

    def create_identity(params)

        body = "{
            \n \"SupplierIdentity\":\"#{params["SupplierIdentity"]}\",
            \"SharedSecret\":\"#{params["SharedSecret"]}\",
            \"BuyerIdentity\":\"#{params["BuyerIdentity"]}\"
            \n
        }"

        response = User.http_request(
            ENV['IDENTITY'],
            User.request_type[:post],
            body
        )
    end

    def edit_identity(params)

        body = "{
            \n
                \"SupplierIdentity\":\"#{params["SupplierIdentity"]}\",
                \"SharedSecret\":\"#{params["SharedSecret"]}\",
                \"BuyerIdentity\":\"#{params["BuyerIdentity"]}\",
                \"TaxNumber\":\"#{params["TaxNumber"]}\",
                \"RegisteredName\":\"#{params["RegisteredName"]}\",
                \"TradingAs\":\"#{params["TradingAs"]}\",
                \"TelephoneNumber\":\"#{params["TelephoneNumber"]}\",
                \"PhysicalID\":\"#{params["PhysicalID"]}\",
                \"PostalID\":\"#{params["PostalID"]}\",
                \"TaxPrefix\":\"#{params["TaxPrefix"]}\",
                \"TaxIndicator\":\"#{params["TaxIndicator"]}\",
                \"TaxPercentage\":\"#{params["TaxPercentage"]}\",
                \"RemitToName\":\"#{params["RemitToName"]}\",
                \"RemitToStreet\":\"#{params["RemitToStreet"]}\",
                \"RemitToCity\":\"#{params["RemitToCity"]}\",
                \"RemitToPostalCode\":\"#{params["RemitToPostalCode"]}\",
                \"RemitToCountryISOCode\":\"#{params["RemitToCountryISOCode"]}\",
                \"RemitToCountry\":\"#{params["RemitToCountry"]}\",
                \"RemitToAreaOrCityCode\":\"#{params["RemitToAreaOrCityCode"]}\",
                \"RemitToPhoneNumber\":\"#{params["RemitToPhoneNumber"]}\",
                \"RemitToFaxNumber\":\"#{params["RemitToFaxNumber"]}\",
                \"RemitToTaxPrefix\":\"#{params["RemitToTaxPrefix"]}\",
                \"RemitToTaxNumber\":\"#{params["RemitToTaxNumber"]}\",
                \"URL\":\"#{params["URL"]}\",
                \"FromName\":\"#{params["FromName"]}\",
                \"FromStreet\":\"#{params["FromStreet"]}\",
                \"FromCity\":\"#{params["FromCity"]}\",
                \"FromPostalCode\":\"#{params["FromPostalCode"]}\",
                \"FromCountryISOCode\":\"#{params["FromCountryISOCode"]}\",
                \"FromCountry\":\"#{params["FromCountry"]}\",
                \"FromTaxPrefix\":\"#{params["FromTaxPrefix"]}\",
                \"FromTaxNumber\":\"#{params["FromTaxNumber"]}\"
            \n
        }"

        response = User.http_request(
            ENV["IDENTITY"],
            User.request_type[:put],
            body
        )
    end

    def delete_identity(id)
        User.http_request(
            "#{ENV['IDENTITY']}/#{id}",
            User.request_type[:delete]
        )
    end

    def p_orders
        response = User.http_request(
            ENV["PURCHASE_ORDERS"],
            User.request_type[:get]
        )
    end

    def po_download(id)
        file_from_http_get("#{ENV['PURCHASE_ORDERS']}/Download/#{id}")
    end

    def po_filter(params)
        User.http_request(
            "#{ENV["PURCHASE_ORDERS"]}?BuyerIdentity=#{params["BuyerIdentity"]}&SupplierIdentity=#{params["SupplierIdentity"]}&OrderID=#{params["OrderID"]}&FromDate=#{params["fromDate"]}&ToDate=#{params["toDate"]}",
            User.request_type[:get]
            )
    end

    def supplier_list
        User.http_request(
            "#{ENV['IDENTITY']}/Suppliers",
            User.request_type[:get]
        )
    end
    
    def buyers_list
        User.http_request(
            "#{ENV['IDENTITY']}/Buyers",
            User.request_type[:get]
        )
    end
    
    def invoice_create(id, params)
        body = "{
            \n
                \"PurchaseOrderID\":\"#{id}\",
                \"InvoiceID\":\"#{params["Invoice"]}\"
            \n
        }"

        User.http_request(
            ENV['INVOICE'],
            User.request_type[:post],
            body
        )
    end

    def invoice_list
        response = User.http_request(
            ENV["INVOICE1"],
            User.request_type[:get]
        )
    end

    def invoice_id_get(id)
        response = User.http_request(
            "#{ENV["INVOICE1"]}/#{id}",
            User.request_type[:get]
        )
    end

    def download_invoice(id)
        file_from_http_get("#{ENV['INVOICE1']}/Download/#{id}")
    end

    def invoice_get(po_id)
        file_from_http_get("#{ENV['INVOICE']}/PurchaseOrder/#{po_id}")
    end

    def invoice_send(invoice_id)
        file_from_http_get("#{ENV['INVOICE']}/#{invoice_id}/SendToCoupa")
    end

    def invoiceItems_of_invoice(id)
        User.http_request(
            "#{ENV["INVOICE_LINE_ITEMS"]}?InvoiceID=#{id}",
            User.request_type[:get]
        )
    end

    def invoiceLineItems_list
        response = User.http_request(
            ENV["INVOICE_LINE_ITEMS"],
            User.request_type[:get]
        )
    end

    def invoiceLineItems_get(id)
        response = User.http_request(
            "#{ENV["INVOICE_LINE_ITEMS"]}/#{id}",
            User.request_type[:get]
        )
    end

    def invoice_item_update(id, invoiceID, params)
        body = "{
            \n
                \"Quantity\":\"#{params["Quantity"]}\",
                \"UnitPrice\":\"#{params["UnitPrice"]}\"
            \n
        }"

        User.http_request(
            "#{ENV["INVOICE"]}/#{invoiceID}/Item/#{id}",
            User.request_type[:put],
            body
        )
    end

    def file_from_http_get(uri)
        url = URI(uri)
        https = Net::HTTP.new(url.host, url.port)
        https.use_ssl = true
    
        request = Net::HTTP::Get.new(url)
        response = https.request(request)
        response.read_body
    end
end