class OrderController < ApplicationController
  before_action :authorize
  
  def index
    @supplier_list = current_user.supplier_list["SupplierIdentities"]
    @buyers_list = current_user.buyers_list["BuyerIdentities"]
    
    if po_filter_params.present?
      orders = current_user.po_filter(po_filter_params)["PurchaseOrders"]

      orders.each do |order|
        if order["OrderDate"].present?
          order["OrderDate"] = time_format(order["OrderDate"])
        end
      end

      @orders = reverse_list(orders)
    else
      orders = current_user.p_orders["PurchaseOrders"]

      orders.each do |order|
        if order["OrderDate"].present?
          order["OrderDate"] = time_format(order["OrderDate"])
        end
      end

      @orders = reverse_list(orders)
    end
  end

  def create
    if po_filter_params.present?
      @orders = po_filter_params
    else
      orders = current_user.p_orders["PurchaseOrders"]

      orders.each do |order|
        if order["OrderDate"].present?
          order["OrderDate"] = time_format(order["OrderDate"])
        end
      end

      @orders = reverse_list(orders)
    end
  end

  def downloadPO
    file = current_user.po_download(params[:id])
    send_data file, filename: "PO_ID_#{params[:id]}.xml", disposition: 'download'
  end
  
  def downloadInvoice
    # WE MUST USE AJAX HERE!!
    unless current_user.invoice_get(params[:id]).include? ("Error")
      file = current_user.invoice_get(params[:id])
      send_data file, filename: "INVOICE_ID_#{params[:id]}.xml", disposition: 'download'
    else
      flash[:danger] = "No Invoice file found. Ensure that you create an Invoice first."
      redirect_to order_index_path
    end
  end

  def invoice;
  end

  def createInvoice
    current_user.invoice_create(params[:id], po_params)
    redirect_to order_index_path
  end

  def sendInvoice
    @invoice = current_user.invoice_send(params[:id])
    
    unless @invoice["Error"].present?
      flash[:success] = 'Invoice successfully sent'
    else
      flash[:danger] = "Ensure that you have created an Invoice before Sending an invoice to Coupa"
    end
    
    redirect_to order_index_path
  end

  private
  
  def time_format(time_string)
    temp_arr = time_string.split('T')
    temp_arr2 = temp_arr[1].split(':')
    temp_str = temp_arr[0] + ", " + temp_arr2[0] + ":" + temp_arr2[1]
    temp_str
  end

  def reverse_list(list)
    list.reverse()
  end

  def po_params
    params.require(:order).permit!
  end

  def po_filter_params
    params.permit(:BuyerIdentity, :SupplierIdentity, :OrderID, :fromDate, :toDate)
  end
end