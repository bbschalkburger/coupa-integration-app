class InvoicesController < ApplicationController
    before_action :authorize
    
    def index
        @invoices = current_user.invoice_list["Invoices"]

        @invoices.each do |invoice|
            if invoice["createDate"].present? || invoice["updateDate"].present?
                invoice["createDate"] = time_format(invoice["createDate"]) if invoice["createDate"].present?
                invoice["updateDate"] = time_format(invoice["updateDate"]) if invoice["updateDate"].present?
            end
        end
    end

    def show
        @invoice = current_user.invoice_id_get(params[:id])["Invoice"]
        @invoice["createDate"] = time_format(@invoice["createDate"]) if @invoice["createDate"].present?
        @invoice["updateDate"] = time_format(@invoice["updateDate"]) if @invoice["updateDate"].present?
    end

    def update;
    end

    def downloadInvoice
        # WE MUST USE AJAX HERE!!
        # purchaseOrderID = current_user.invoice_id_get(paras[:invoice_id])["Invoice"]["purchaseOrderID"]
        
        unless current_user.download_invoice(params[:invoice_id]).include? ("Error")
            file = current_user.download_invoice(params[:invoice_id])
            send_data file, filename: "INVOICE_ID_#{params[:invoice_id]}.xml", disposition: 'download'
            # redirect_to invoices_path
        else
            flash[:danger] = "No Invoice file found. Ensure that you create an Invoice first."
            redirect_to invoices_path
        end
    end

    def sendInvoice
        @invoice = current_user.invoice_send(params[:invoice_id])
        unless @invoice["Error"].present?
          flash[:success] = 'Invoice successfully sent'
        else
          flash[:danger] = "Ensure that you have created an Invoice before Sending an invoice to Coupa"
        end
        redirect_to invoices_path
    end

    private

    def time_format(time_string)
        temp_arr = time_string.split('T')
        temp_arr2 = temp_arr[1].split(':')
        temp_str = temp_arr[0] + ", " + temp_arr2[0] + ":" + temp_arr2[1]
        temp_str
    end 
end
