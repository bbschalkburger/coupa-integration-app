class SessionController < ApplicationController

  def new;
  end

  def create
    begin
      user = User.authentication(require_params[:email], require_params[:password])
      if privilege_access(user['privileges'])
        set_current_user(user)
        redirect_to order_index_path
      else
        flash[:danger] = "Unauthorized Permission"
        redirect_to root_path
      end
    rescue StandardError
      flash[:danger] = "Invalid Username / Password. Try Again"
      redirect_to root_path
    end
  end

  def destroy
    reset_session
    redirect_to root_path
  end

  private

  def require_params
    params.permit(:email, :password, :authenticity_token)
  end

  def privilege_access(privileges)
    !!privileges.detect { |privilege| privilege['Code'] == 'SYS' }
  end
end
