class ApplicationController < ActionController::Base
    helper_method :authorize
    helper_method :current_user
    helper_method :logged_in?

    rescue_from ActionController::InvalidAuthenticityToken, with: :user_not_authorized

    def user_not_authorized
        redirect_to root_path
    end

    def logged_in?
        !current_user.nil?
    end

    def redirect_if_logged_in?
        redirect_to order_index_path if logged_in?
    end

    def current_user
        @current_user ||= begin
            User.new(session[:user]) if session[:user]
        end
    end

    def authorize
        return if logged_in?
        session[:return_to] = request.url
        redirect_to root_path
    end

    def set_current_user(user)
        session[:user] = user
    end

end
