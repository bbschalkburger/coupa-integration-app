class IdentityController < ApplicationController
  before_action :authorize

  def new;
  end

  def create
    current_user.create_identity(identity_params)
    redirect_to identity_index_path
  end

  def update
    current_user.edit_identity(identity_params)
    redirect_to identity_index_path
  end

  def edit
    @identity = current_user.get_identity(params[:id])
  end

  def show
    @identity = current_user.get_identity(params[:id])

    if @identity["CreateDate"].present?
      @identity["CreateDate"] = time_format( current_user.get_identity(params[:id])["CreateDate"] )
    end
  end

  def index
    @identities = current_user.identities["Identities"]

    @identities.each do |identity|
      if identity["CreateDate"].present?
        identity["CreateDate"] = time_format(identity["CreateDate"])
      end
    end
  end

  def destroy
    current_user.delete_identity(params[:id])
    redirect_to identity_index_path
  end

  private

  def identity_params
    params.require(:identity).permit!
  end

  def time_format(time_string)
    temp_arr = time_string.split('T')
    temp_arr2 = temp_arr[1].split(':')
    temp_str = temp_arr[0] + ", " + temp_arr2[0] + ":" + temp_arr2[1]
    temp_str
  end 
end
