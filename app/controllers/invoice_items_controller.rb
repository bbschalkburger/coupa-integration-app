class InvoiceItemsController < ApplicationController
  before_action :authorize
  
  def index
    @invoice_items = current_user.invoiceItems_of_invoice(params[:invoice_id])["InvoiceItems"]
  end

  def show
    @invoice_item = current_user.invoiceLineItems_get(params[:id])["InvoiceItem"]
    @invoice_item["createDate"] = time_format(@invoice_item["createDate"]) if @invoice_item["createDate"].present?
    @invoice_item["updateDate"] = time_format(@invoice_item["updateDate"]) if @invoice_item["updateDate"].present?
  end

  def edit
    @invoice_item = current_user.invoiceLineItems_get(params[:id])["InvoiceItem"]
  end

  def update
    invoiceID = current_user.invoiceLineItems_get(params[:id])["InvoiceItem"]["invoiceID"]
    @response = current_user.invoice_item_update( params[:id], invoiceID, invoice_items_params )
    redirect_to invoice_invoice_items_path(invoiceID: @response["invoiceID"])
  end

  private

  def invoice_items_params
    params.require(:invoice_item).permit!
  end

  def time_format(time_string)
    temp_arr = time_string.split('T')
    temp_arr2 = temp_arr[1].split(':')
    temp_str = temp_arr[0] + ", " + temp_arr2[0] + ":" + temp_arr2[1]
    temp_str
  end 
end