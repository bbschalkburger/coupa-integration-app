window.addEventListener("load", () => {
    const links = document.querySelectorAll("a[data-remote]");
    links.forEach((element) => {

      element.addEventListener("ajax:beforeSend", () => {
        alert("Loading...");
      });

      element.addEventListener("ajax:success", () => {
        alert("No Invoice file found. Ensure that you create an Invoice first.");
      });
    });
  });
