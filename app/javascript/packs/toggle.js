document.getElementById("password_toggle").addEventListener("click", PasswordToggle)

function PasswordToggle()
{
    let passwordInput = document.getElementById('floatingPassword')
    let pwToggle = document.getElementById("password_toggle")

    if (passwordInput.getAttribute('type') === 'password' && pwToggle.classList.contains("bi-eye-fill")){
        passwordInput.setAttribute('type', 'text');
        pwToggle.classList.replace("bi-eye-fill", "bi-eye-slash-fill")
    }
    else {
        passwordInput.setAttribute('type', 'password');
        pwToggle.classList.replace("bi-eye-slash-fill", "bi-eye-fill");
    }
}

// if (x.classList.contains("mystyle")) {
//     x.classList.replace("mystyle", "mystyle1");
// } else if (x.classList.contains("mystyle1")) {
//     x.classList.replace("mystyle1", "mystyle");
// }