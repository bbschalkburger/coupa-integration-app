require "test_helper"

class IdentityControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get identity_index_url
    assert_response :success
  end

  test "should get show" do
    get identity_show_url
    assert_response :success
  end

  test "should get edit" do
    get identity_edit_url
    assert_response :success
  end

  test "should get update" do
    get identity_update_url
    assert_response :success
  end

  test "should get create" do
    get identity_create_url
    assert_response :success
  end

  test "should get destroy" do
    get identity_destroy_url
    assert_response :success
  end
end
