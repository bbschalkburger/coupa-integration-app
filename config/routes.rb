Rails.application.routes.draw do
  root 'session#new'
  
  post 'login', to: 'session#create' # I will get to it when there's authenctication
  delete 'logout', to: 'session#destroy'
  
  get 'download_po/:id', to: 'order#downloadPO', as: 'download_po'

  get 'order/invoice/:id', to: 'order#invoice', as: 'order_invoice'
  post 'order/invoice/:id', to: 'order#createInvoice', as: 'create_invoice'
  post 'order', to: 'order#index'
  
  resources :identity
  resources :order
  resources :invoices do
    resources :invoice_items
    get 'download', to: 'invoices#downloadInvoice'
    get 'send', to: 'invoices#sendInvoice'
  end

  # get 'invoice_items/index'
  # get 'invoice_items/show'
  # get 'invoice_items/edit'
  # get 'invoice_items/update'
  # get 'invoice_items/index'
  # get 'invoice_items/edit'
  # get 'invoice_items/update'
  # get 'invoice_items/show'  
  get 'download_invoice/:id', to: 'order#downloadInvoice', as: 'download_invoice'
  get 'send_invoice/:id', to: 'order#sendInvoice', as: 'send_invoice'
  # resources :invoice_items
  # get 'invoices/:id/invoice_items', to: 'invoices#invoice_items_list', as: 'invoice_items_list'
  # post 'order_filter', to: 'order#index'

  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
